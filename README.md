# barcode scanner
Forked from https://gitlab.com/ui5-barcode/barcode-scanner/
Trying to get it working on iOS Browser and PoC if this really can replace the Fiori Client.

Fooling around with ways of JS based Barcode Scanning - not intended for productive use.

Barcode scanner control for UI5. This is a custom UI5 control for a barcode scanner. It uses Quagga for barcode reading and supports 1d codes.
