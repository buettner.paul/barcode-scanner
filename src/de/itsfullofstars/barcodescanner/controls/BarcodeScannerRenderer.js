/*!
 * Tobias Hofmann
 * Contact: https://www.itsfullofstars.de
 */

sap.ui.define([],

	function () {
		"use strict";

		/**
		 * BarcodeScanner renderer.
		 * @namespace
		 */
		var BarcodeScannerRenderer = {
			"apiVersion": 2
		};

		/**
		 * Renders the HTML for the given control, using the provided
		 * {@link sap.ui.core.RenderManager}.
		 *
		 * @param {sap.ui.core.RenderManager} oRm
		 *            the RenderManager that can be used for writing to
		 *            the Render-Output-Buffer
		 * @param {sap.ui.core.Control} oControl
		 *            the control to be rendered
		 */
		BarcodeScannerRenderer.render = function (oRm, oControl) {

			oRm.openStart("div", oControl).style("width", "100%").style("height", "100%").openEnd();

			// for overlay of focus image / brackets
			oRm.openStart("div", oControl).class("videobox").style("width", "100%").style("height", "100%").openEnd().close("div");
			oRm.write("<div style='display: flex; flex-direction: row; align-items: center; justify-content: space-around;'>");
			// canvas for captured image
			oRm.write(
				"<canvas width='%w' height='%h' style='width: %pw; height: %ph;'></canvas>"
				.replace("%w", oControl.getVideoWidth())
				.replace("%h", oControl.getVideoHeight())
				.replace("%pw", oControl.getWidth())
				.replace("%ph", oControl.getHeight()));
			oRm.write("</div>");
			// camera
			oRm.write(
				"<video width='%w' height='%h' autoplay='true' preload='true' muted='true' style='width: %pw; height: %ph;'></video>"
				.replace("%w", oControl.getVideoWidth())
				.replace("%h", oControl.getVideoHeight())
				.replace("%pw", oControl.getWidth())
				.replace("%ph", oControl.getHeight())
			);
			oRm.close("div");
		};

		return BarcodeScannerRenderer;

	}, /* bExport= */ true);