var jsValidator = require("@sap/di.code-validation.js");
var xmlValidator = require("@sap/di.code-validation.xml");

module.exports = function (grunt) {
	"use strict";

	grunt.loadNpmTasks("grunt-run");
	grunt.initConfig({
		run: {
			options: {
				failOnError: true
			},
			build: {
				exec: "npm run build-for-deploy"
			},
			clean: {
				exec: "npm run clean"
			}
		}
	});

	grunt.registerTask("default", ["run:clean", "run:build"]);

};